# Short-link Frontend

Frontend-часть сервиса по сокращению ссылок.

![Preview](./preview.png)

## Интеграция с backend-частью
Интеграция выполнена с [данным backend-приложением](https://gitlab.com/levkovalenko/shortlinkdemon).
