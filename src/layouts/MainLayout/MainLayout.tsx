import React from "react";
import "./MainLayout.scss";
import * as L from "korus-ui";
import { REACT_APP_SITE_TITLE } from "../../config";

type Props = {
    children: React.ReactNode;
};

function MainLayout(props: Props): JSX.Element {
    const { children } = props;

    return (
        <L.Div _MainLayout>
            <L.Div _MainLayout-left>
                <L.H1 _MainLayout-title>
                    {REACT_APP_SITE_TITLE}
                </L.H1>
                <L.P _MainLayout-description>
                    Довольно часто при общении в интернете люди пересылают друг другу ссылки.
                    Иногда эти ссылки бывают длинными. Совсем иногда — очень длинными.
                    Но почти всегда — слишком длинными. Для того чтобы избавиться от этой проблемы,
                    был создан этот сервис.
                </L.P>
                <L.P _MainLayout-description>
                    А дальше все просто: вставляете ссылку в поле для ввода, нажимаете кнопку и
                    получаете короткий, совсем короткий URL.
                </L.P>
                <L.P _MainLayout-description>
                    Иногда (нередко) данным сервисом пользуются злоумышленники: хакеры, крекеры,
                    спамы, куки, закладки, троянские кони… Очевидно, что это противоречит всем
                    правилам использования сервиса, а также здравому смыслу. Если вы столкнулись
                    с такой cитуацией, пожалуйста, напишите письмо нашим специалистам.
                </L.P>
            </L.Div>
            <L.Div _MainLayout-right>
                {children}
            </L.Div>
        </L.Div>
    );
}

export default MainLayout;
