import React from "react";
import "./Form.scss";
import * as L from "korus-ui";

type Props = {
    onSubmit: (url: string) => void;
    isLoading: boolean;
};

function Form(props: Props): JSX.Element {
    const { onSubmit, isLoading } = props;

    const onClick = (ev: L.ButtonTypes.SubmitEvent) => {
        if (ev.form) {
            const { value } = ev.form.form.url;
            if (value) {
                onSubmit(value);
            }
        }

        L.form("form").reset();
    };

    return (
        <L.Div _Form>
            <L.Input
                _Form-input
                form="form"
                name="url"
                isRequired
                validator="url"
                invalidMessage="Некорректный URL"
                placeholder="Введите URL"
            />
            <L.Button
                _Form-button
                form="form"
                type="submit"
                onClick={onClick}
                isLoading={isLoading}
            >
                Сгенерировать
            </L.Button>
        </L.Div>
    );
}

export default Form;
