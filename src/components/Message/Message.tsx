import React, { useState } from "react";
import "./Message.scss";
import * as L from "korus-ui";
import { ReactComponent as IconCopy } from "./icons/IconCopy.svg";
import { TErrorType } from "../../types";

type Props = {
    originalUrl: string;
    newUrl: string;
    isError: boolean;
    errorType: TErrorType;
};

function Message(props: Props): JSX.Element {
    const [isCopied, setIsCopied] = useState(false);
    const {
        originalUrl, newUrl, isError, errorType
    } = props;

    const getTitle = (): string => {
        if (!isError) {
            return "Ссылка сгенерирована";
        }
        return errorType === "connect" ? "Ошибка подключения" : "Стек вызовов переполнен";
    };

    const getDescription = (): string => {
        if (!isError) {
            return `Исходный URL: ${originalUrl}`;
        }
        return errorType === "connect"
            ? "Сервис недоступен. Проверьте свое подключение к сети. И попробуйте снова."
            : "Стек вызовов переполнен. Подождите несколько минут и попробуйте снова.";
    };

    const onClick = () => {
        setIsCopied(false);
        navigator.clipboard.writeText(newUrl).then(() => {
            setIsCopied(true);
        });
    };

    return (
        <L.Div _Message>
            <L.Div
                _Message-status
                _Message_error={isError}
            />
            <L.Div _Message-title>
                {getTitle()}
            </L.Div>
            <L.Div _Message-description>
                {getDescription()}
            </L.Div>
            {!isError && (
                <L.Div _Message-footer>
                    <L.Input
                        _Message-url
                        form="copyForm"
                        name="url"
                        value={newUrl}
                    />
                    <L.Button
                        _Message-button
                        form="copyForm"
                        onClick={onClick}
                    >
                        <IconCopy
                            width={14}
                            height={14}
                        />
                    </L.Button>
                </L.Div>
            )}
            {isCopied && <L.Span _Message-info>Ссылка скопирована</L.Span>}
        </L.Div>
    );
}

export default Message;
