import assertProcessEnv from "./utils/assertProcessEnv";

// this file contains all global constants

export const REACT_APP_API_URL = assertProcessEnv("REACT_APP_API_URL");

export const REACT_APP_SITE_TITLE = assertProcessEnv("REACT_APP_SITE_TITLE");
