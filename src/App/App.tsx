import React, { useState } from "react";
import "./App.scss";
import * as L from "korus-ui";
import { AxiosError } from "axios";
import axiosInst from "../remote/axiosInst";
import MainLayout from "../layouts/MainLayout/MainLayout";
import Form from "../components/Form/Form";
import Message from "../components/Message/Message";
import { TMessage, TResponse } from "../types";

function App(): JSX.Element {
    const [messages, setMessages] = useState<TMessage[]>([]);
    const [isLoading, setIsLoading] = useState(false);

    const onSubmit = (url: string) => {
        setIsLoading(true);
        const newMessages = [...messages];
        axiosInst.post<TResponse>("/shortlink", { link: url }).then((res) => {
            if (res.status === 200) {
                newMessages.unshift({
                    id: Date.now().toString(),
                    originalUrl: url,
                    newUrl: res.data.shortLink,
                    isError: false,
                    errorType: "none"
                });
                setMessages(newMessages);
            }
        }).catch((err: AxiosError) => {
            if (err.response && err.response.status === 429) {
                newMessages.unshift({
                    id: Date.now().toString(),
                    originalUrl: "",
                    newUrl: "",
                    isError: true,
                    errorType: "stack"
                });
            }
            else {
                newMessages.unshift({
                    id: Date.now().toString(),
                    originalUrl: "",
                    newUrl: "",
                    isError: true,
                    errorType: "connect"
                });
            }
            setMessages(newMessages);
        }).finally(() => {
            setIsLoading(false);
        });
    };

    return (
        <MainLayout>
            <Form
                onSubmit={onSubmit}
                isLoading={isLoading}
            />
            <L.Div _Messages>
                {messages.map((item) => (
                    <Message
                        key={item.id}
                        originalUrl={item.originalUrl}
                        newUrl={item.newUrl}
                        isError={item.isError}
                        errorType={item.errorType}
                    />
                ))}
            </L.Div>
        </MainLayout>
    );
}

export default App;
