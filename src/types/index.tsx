export type TErrorType = "connect" | "stack" | "none";

export type TMessage = {
    id: string;
    originalUrl: string;
    newUrl: string;
    isError: boolean;
    errorType: "connect" | "stack" | "none";
};

export type TResponse = {
    shortLink: string;
};
